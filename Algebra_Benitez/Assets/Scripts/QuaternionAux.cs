﻿using System;
using System.ComponentModel;
using UnityEngine;

namespace CustomMath
{
    public class QuaternionAux : IEquatable<QuaternionAux>
    {
        #region Variables
        public float x;
        public float y;
        public float z;
        public float w;

        const float radToDeg = (180.0f / Mathf.PI);
        const float degToRad = (Mathf.PI / 180.0f);

        // Devuelve el cuaternión con una magnitud de 1
        public QuaternionAux normalized { get { return Normalize(this); } }

        // Accede a los componentes x, y, z, w usando [0], [1], [2], [3] respectivamente
        public float this[int index]
        {
            get
            {
                switch (index)
                {
                    case 0: return x;
                    case 1: return y;
                    case 2: return z;
                    case 3: return w;
                    default:
                        throw new IndexOutOfRangeException("Invalid quaternion index");
                }
            }
            set
            {
                switch (index)
                {
                    case 0: x = value; break;
                    case 1: y = value; break;
                    case 2: z = value; break;
                    case 3: w = value; break;
                    default:
                        throw new IndexOutOfRangeException("Invalid quaternion index");
                }
            }
        }
        #endregion

        #region constants
        public const float kEpsilon = 1E-06F;
        #endregion

        #region Default Values
        public static QuaternionAux identity { get { return new QuaternionAux(0.0f, 0.0f, 0.0f, 1.0f); } }
        #endregion

        #region Constructor
        public QuaternionAux(float x, float y, float z, float w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }
        public QuaternionAux(QuaternionAux q)
        {
            x = q.x;
            y = q.y;
            z = q.z;
            w = q.w;
        }
        #endregion

        #region Operators
        public static Vector3 operator *(QuaternionAux rotation, Vector3 point)
        {
            float x = rotation.x * 2.0F;
            float y = rotation.y * 2.0F;
            float z = rotation.z * 2.0F;
            float xx = rotation.x * x;
            float yy = rotation.y * y;
            float zz = rotation.z * z;
            float xy = rotation.x * y;
            float xz = rotation.x * z;
            float yz = rotation.y * z;
            float wx = rotation.w * x;
            float wy = rotation.w * y;
            float wz = rotation.w * z;

            Vector3 res;
            res.x = (1F - (yy + zz)) * point.x + (xy - wz) * point.y + (xz + wy) * point.z;
            res.y = (xy + wz) * point.x + (1F - (xx + zz)) * point.y + (yz - wx) * point.z;
            res.z = (xz - wy) * point.x + (yz + wx) * point.y + (1F - (xx + yy)) * point.z;
            return res;
        }
        public static QuaternionAux operator *(QuaternionAux lhs, QuaternionAux rhs)
        {
            return new QuaternionAux
            (
                lhs.w * rhs.x + lhs.x * rhs.w + lhs.y * rhs.z - lhs.z * rhs.y,
                lhs.w * rhs.y + lhs.y * rhs.w + lhs.z * rhs.x - lhs.x * rhs.z,
                lhs.w * rhs.z + lhs.z * rhs.w + lhs.x * rhs.y - lhs.y * rhs.x,
                lhs.w * rhs.w - lhs.x * rhs.x - lhs.y * rhs.y - lhs.z * rhs.z
            );
        }
        public static bool operator ==(QuaternionAux lhs, QuaternionAux rhs)
        {
            return (lhs == rhs);
        }
        public static bool operator !=(QuaternionAux lhs, QuaternionAux rhs)
        {
            return !(lhs == rhs);
        }
        #endregion

        #region Functions
        //Devuelve el ángulo en grados entre dos rotaciones a y b.
        public static float Angle(QuaternionAux a, QuaternionAux b)
        {
            float f = Dot(a, b);
            return Mathf.Acos(Mathf.Min(Mathf.Abs(f), 1f)) * 2f * radToDeg;
        }

        // Crea una rotación que rota en grados de ángulo alrededor del eje
        public static QuaternionAux AngleAxis(float angle, Vector3 axis)
        {
            if (axis.sqrMagnitude == 0)
                return identity;

            QuaternionAux result = identity;
            var radians = angle * degToRad;
            radians *= 0.5f;
            axis.Normalize();
            axis = axis * Mathf.Sin(radians);
            result.x = axis.x;
            result.y = axis.y;
            result.z = axis.z;
            result.w = Mathf.Cos(radians);

            return Normalize(result);
        }

        // El producto punto/escalar entre dos rotaciones
        public static float Dot(QuaternionAux a, QuaternionAux b)
        {
            return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
        }

        // Devuelve una rotación que gira Z grados alrededor del eje Z, X grados alrededor del eje X e Y grados alrededor del eje Y (aplicado en ese orden)
        public static QuaternionAux Euler(Vector3 euler)
        {
            QuaternionAux qX = identity;
            QuaternionAux qY = identity;
            QuaternionAux qZ = identity;

            float sin = Mathf.Sin(Mathf.Deg2Rad * euler.x * 0.5f);
            float cos = Mathf.Cos(Mathf.Deg2Rad * euler.x * 0.5f);
            qX.Set(sin, 0.0f, 0.0f, cos);

            sin = Mathf.Sin(Mathf.Deg2Rad * euler.y * 0.5f);
            cos = Mathf.Cos(Mathf.Deg2Rad * euler.y * 0.5f);
            qY.Set(0.0f, sin, 0.0f, cos);

            sin = Mathf.Sin(Mathf.Deg2Rad * euler.z * 0.5f);
            cos = Mathf.Cos(Mathf.Deg2Rad * euler.z * 0.5f);
            qZ.Set(0.0f, 0.0f, sin, cos);

            return new QuaternionAux(qX * qY * qZ);
        }
        public static QuaternionAux Euler(float x, float y, float z)
        {
            return Euler(new Vector3(x, y, z));
        }

        // Crea una rotación que gira de fromDirection a toDirection
        public static QuaternionAux FromToRotation(Vector3 fromDirection, Vector3 toDirection)
        {
            return RotateTowards(LookRotation(fromDirection), LookRotation(toDirection), float.MaxValue);
        }

        // Devuelve el inverso de la rotación
        public static QuaternionAux Inverse(QuaternionAux rotation)
        {
            float lengthSq = rotation.x * rotation.x + rotation.y * rotation.y + rotation.z * rotation.z + rotation.w * rotation.w;
            if (lengthSq != 0.0)
            {
                float i = 1.0f / lengthSq;
                Vector3 vec = new Vector3(rotation.x, rotation.y, rotation.z) * -i;
                return new QuaternionAux(vec.x, vec.y, vec.z, rotation.w * i);
            }
            return rotation;
        }

        // Interpola entre a y b por t y normaliza el resultado. El parámetro t está sujeto al rango [0, 1]
        public static QuaternionAux Lerp(QuaternionAux a, QuaternionAux b, float t)
        {
            t = Mathf.Clamp(t, 0, 1);
            return LerpUnclamped(a, b, t);
        }

        // Interpola entre a y b por t y normaliza el resultado. El parámetro t no esta sujeto a ningun rango
        public static QuaternionAux LerpUnclamped(QuaternionAux a, QuaternionAux b, float t)
        {
            QuaternionAux resultInterpolated = identity;

            if (t >= 1)
                resultInterpolated = b;
            else if (t <= 0)
                resultInterpolated = a;
            else
            {
                QuaternionAux difference = new QuaternionAux(b.x - a.x, b.y - a.y, b.z - a.z, b.w - b.w);
                QuaternionAux differenceLerped = new QuaternionAux(difference.x * t, difference.y * t, difference.z * t, difference.w * t);

                resultInterpolated = new QuaternionAux(a.x + differenceLerped.x, a.y + differenceLerped.y, a.z + differenceLerped.z, a.w + differenceLerped.w);
            }
            return resultInterpolated.normalized;
        }

        // Crea una rotación con las direcciones hacia adelante y hacia arriba especificadas
        public static QuaternionAux LookRotation(Vector3 forward)
        {
            return LookRotation(forward, Vector3.up);
        } 
        public static QuaternionAux LookRotation(Vector3 forward, [DefaultValue("Vector3.up")] Vector3 upwards)
        {
            forward = Vector3.Normalize(forward);
            Vector3 right = Vector3.Normalize(Vector3.Cross(upwards, forward));
            upwards = Vector3.Cross(forward, right);
            float m00 = right.x;
            float m01 = right.y;
            float m02 = right.z;
            float m10 = upwards.x;
            float m11 = upwards.y;
            float m12 = upwards.z;
            float m20 = forward.x;
            float m21 = forward.y;
            float m22 = forward.z;


            float num8 = (m00 + m11) + m22;
            QuaternionAux quaternion = identity;
            if (num8 > 0f)
            {
                var num = (float)Math.Sqrt(num8 + 1f);
                quaternion.w = num * 0.5f;
                num = 0.5f / num;
                quaternion.x = (m12 - m21) * num;
                quaternion.y = (m20 - m02) * num;
                quaternion.z = (m01 - m10) * num;
                return quaternion;
            }
            if ((m00 >= m11) && (m00 >= m22))
            {
                var num7 = (float)Math.Sqrt(((1f + m00) - m11) - m22);
                var num4 = 0.5f / num7;
                quaternion.x = 0.5f * num7;
                quaternion.y = (m01 + m10) * num4;
                quaternion.z = (m02 + m20) * num4;
                quaternion.w = (m12 - m21) * num4;
                return quaternion;
            }
            if (m11 > m22)
            {
                var num6 = (float)Math.Sqrt(((1f + m11) - m00) - m22);
                var num3 = 0.5f / num6;
                quaternion.x = (m10 + m01) * num3;
                quaternion.y = 0.5f * num6;
                quaternion.z = (m21 + m12) * num3;
                quaternion.w = (m20 - m02) * num3;
                return quaternion;
            }
            var num5 = (float)Math.Sqrt(((1f + m22) - m00) - m11);
            var num2 = 0.5f / num5;
            quaternion.x = (m20 + m02) * num2;
            quaternion.y = (m21 + m12) * num2;
            quaternion.z = 0.5f * num5;
            quaternion.w = (m01 - m10) * num2;
            return quaternion;
        }

        // Convierte este cuaternión en uno con la misma orientación pero con una magnitud de 1
        public static QuaternionAux Normalize(QuaternionAux q)
        {
            float mag = Mathf.Sqrt(Dot(q, q));

            if (mag < Mathf.Epsilon)
                return identity;

            return new QuaternionAux(q.x / mag, q.y / mag, q.z / mag, q.w / mag);
        }

        // Gira una rotación from hacia to
        public static QuaternionAux RotateTowards(QuaternionAux from, QuaternionAux to, float maxDegreesDelta)
        {
            float angle = Angle(from, to);
            if (angle == 0.0f) return to;
            return SlerpUnclamped(from, to, Mathf.Min(1.0f, maxDegreesDelta / angle));
        }

        // Igual que lerp pero interpola esfericamente
        public static QuaternionAux Slerp(QuaternionAux a, QuaternionAux b, float t)
        {
            Mathf.Clamp(t, 0, 1f);
            return SlerpUnclamped(a, b, t);
        }

        // Igual que lerp unclamped pero interpola esfericamente
        public static QuaternionAux SlerpUnclamped(QuaternionAux a, QuaternionAux b, float t)
        {
            QuaternionAux quatInterpolated = identity;

            float cosHalfTheta = Dot(a, b);
            if (Mathf.Abs(cosHalfTheta) >= 1f)
            {
                quatInterpolated.Set(a.x, a.y, a.z, a.w);
                return quatInterpolated;
            }
            float halfTheta = Mathf.Acos(cosHalfTheta);
            float sinHalfTheta = Mathf.Sqrt(1 - cosHalfTheta * cosHalfTheta);
            if (Mathf.Abs(sinHalfTheta) < 0.001f)
            {
                quatInterpolated.w = (a.w * 0.5f + b.w * 0.5f);
                quatInterpolated.x = (a.x * 0.5f + b.x * 0.5f);
                quatInterpolated.y = (a.y * 0.5f + b.y * 0.5f);
                quatInterpolated.z = (a.z * 0.5f + b.z * 0.5f);
                return quatInterpolated;
            }
            float ratioA = Mathf.Sin((1 - t) * halfTheta) / sinHalfTheta;
            float ratioB = Mathf.Sin(t * halfTheta) / sinHalfTheta;
            quatInterpolated.w = (a.w * ratioA + b.w * ratioB);
            quatInterpolated.x = (a.x * ratioA + b.x * ratioB);
            quatInterpolated.y = (a.y * ratioA + b.y * ratioB);
            quatInterpolated.z = (a.z * ratioA + b.z * ratioB);
            return quatInterpolated;
        }
        #endregion

        #region Internals
        public bool Equals(QuaternionAux other)
        {
            return x.Equals(other.x) && y.Equals(other.y) && z.Equals(other.z) && w.Equals(other.w);
        }
        public override bool Equals(object other)
        {
            if (!(other is QuaternionAux)) return false;
            return Equals((QuaternionAux)other);
        }

        public override int GetHashCode()
        {
            return x.GetHashCode() ^ (y.GetHashCode() << 2) ^ (z.GetHashCode() >> 2) ^ (w.GetHashCode() >> 1);
        }

        public void Normalize()
        {
            QuaternionAux quaternion = this;
            quaternion = Normalize(this);
        }

        // Establece los componentes x, y, z y w de un cuaternion existente
        public void Set(float newX, float newY, float newZ, float newW)
        {
            x = newX;
            y = newY;
            z = newZ;
            w = newW;
        }

        // Crea una rotación que gira de fromDirection a toDirection
        public void SetFromToRotation(Vector3 fromDirection, Vector3 toDirection)
        {
            QuaternionAux quaternion = this;
            quaternion = FromToRotation(fromDirection, toDirection);
        }

        // Crea una rotación con las direcciones hacia adelante y hacia arriba especificadas
        public void SetLookRotation(Vector3 view)
        {
            SetLookRotation(view, Vector3.up);
        }
        public void SetLookRotation(Vector3 view, [DefaultValue("Vector3.up")] Vector3 up)
        {
            QuaternionAux quaternion = this;
            quaternion = LookRotation(view, up);
        }

        // Convierte una rotación en una representación de eje de ángulo (ángulos en grados)
        public void ToAngleAxis(out float angle, out Vector3 axis)
        {
            if (Math.Abs(w) > 1.0f)
                Normalize();

            angle = 2.0f * Mathf.Acos(w);
            float den = Mathf.Sqrt(1.0f - w * w);
            if (den > 0.0001f) axis = new Vector3(x, y, z) / den;
            else axis = new Vector3(1, 0, 0);

            angle *= radToDeg;
        }

        // Devuelve una cadena formateada del cuaternion
        public string ToString(string format)
        {
            return "X = " + x.ToString(format) + " Y = " + y.ToString(format) + " Z = " + z.ToString(format) + " W = " + w.ToString(format);
        }
        public override string ToString()
        {
            return "X = " + x.ToString() + " Y = " + y.ToString() + " Z = " + z.ToString() + " W = " + w.ToString();
        }
        #endregion
    }
}